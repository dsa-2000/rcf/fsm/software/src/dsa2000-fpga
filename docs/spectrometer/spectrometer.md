# Introduction

This document describes how to set up the hardware and software for the iWave FPGA spectrometer system.

# Setting up the Hardware

## Required Hardware

The following hardware is required to set up the iWave FPGA spectrometer system:

- [iWave ZU11 SoM Development Kit](https://www.iwavesystems.com/product/zu19-zu17-zu11-zynq-ultrascale-mpsocsom/)
- iWave ZU11 SoM Development Kit 12V power supply & AC power cord
- [Analog Devices AD9082-FMC-EBZ evaluation board](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/eval-ad9082.html)
- Full size SD card (at least 16 GB capacity)
- RJ45 Ethernet cable (for connecting the iWave ZU11 SoM Development Kit to a network)
- USB A to USB B micro cable (for connecting the iWave ZU11 SoM Development Kit to a computer)
- 3/32" diameter standoffs (for ADC FMC card)

## Hardware Setup

![iWave ZU11 SoM Development Kit and Analog Devices AD9082-FMC-EBZ evaluation board](../_static/figures/iwave-ad9082.jpg)
*An iWave ZU11 SoM Development Kit and an Analog Devices AD9082-FMC-EBZ evaluation board connected via an FMC+ interface*

![iWave ZU11 SoM Development Kit and Analog Devices AD9082-FMC-EBZ evaluation board](../_static/figures/iwave-ad9082-annotated.png)
*An iWave ZU11 SoM Development Kit and an Analog Devices AD9082-FMC-EBZ evaluation board connected via an FMC+ interface*


1. Unpack the iWave ZU11 SoM Development Kit and install the heatsink and fan. Be sure to follow the iWave installation instructions. In particular, take care to connect the fan power cable to the correct power header.
2. Unpack the Analog Devices AD9082-FMC-EBZ evaluation board and install the heatsink and fan, following the Analog Devices installation instructions.
3. Connect the AD9082-FMC-EBZ evaluation board to the iWave ZU11 SoM Development Kit using the FMC+ connector (with reference designator J14). Note that the FMC+ connector on the iWave development board has more pins than the ADC card. Do not mistake it for the FMC connector (J22)!
4. Connect the iWave ZU11 SoM Development Kit to a computer using a USB A to USB B micro cable, using the USB port indicated in the above figure. On the PCB silkscreen, this port is labeled "DEBUG UART CONN" and is connector J13.
5. Connect the iWave ZU11 SoM Development Kit to a network using the port indicated in the above figure. On the PCB silkscreen, this port is labeled "ETHERNET CONN 1" and is connector J17.
6. Configure the boot mode switches (see above figure) on the iWave SoM to boot from the SD card. The switches should be set as follows:
   - Switch 1: OFF
   - Switch 2: ON
   - Switch 3: OFF
   - Switch 4: ON

## Creating a SD Card Boot Image
