#!/usr/bin/env python3

HOST = '10.11.0.204'
FPGFILE = '/home/jackh/src/dsa2000-fpga/firmware/src/models/dsa2k_iwave_rfi/outputs/dsa2k_iwave_rfi_2024-12-04_1344.fpg'

import casperfpga
import rtr_fpga

print(f'Connecting to {HOST}')
cfpga = casperfpga.CasperFpga(HOST, transport=casperfpga.KatcpTransport)
print(f'Loading firmware {FPGFILE}')
r = rtr_fpga.RtrDspSystem(cfpga, FPGFILE)
r.program()

print(f'Initializing')
r.initialize(read_only=False)

r.print_status_all()

print('Plotting spectra')
r.autocorr_maxhold.plot_spectra()
