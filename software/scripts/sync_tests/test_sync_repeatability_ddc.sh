#!/bin/bash

PING_TIMEOUT=120
FPGFILE=../../../firmware/src/models/dsa2k_iwave_adc_only_sync_test_ddc/outputs/dsa2k_iwave_adc_only_sync_test_ddc_2024-09-05_1020.fpg

function boot_and_test() {
    echo "Rebooting $1"
    ssh casper@$1 sudo reboot
    echo "Rebooting $2"
    ssh casper@$2 sudo reboot
    sleep 5
    ./wait_for_server.sh $1 $PING_TIMEOUT
    ./wait_for_server.sh $2 $PING_TIMEOUT
    # Wait another 5 seconds to ensure ADCs are configured by startup scripts
    sleep 5
    echo "Running test"
    ./dsa2k_f_sync_test.py --fpgfile $FPGFILE --hosts $1,$2 --sync_delay $3 --plot --complexfft --lo_mhz 1350 --fpga_clk_hz 200000000
    echo $1
    ssh casper@$1 sudo python read_jesd_rx_axi.py
    echo $2
    ssh casper@$2 sudo python read_jesd_rx_axi.py
}

# Usage: test_sync_repeatability.sh <nloop> <host1> <host2> <lmfc_delay>

echo "Running $1 trials"
for (( i=1; i<=$1; i++ )); do
    echo "Trial $i"
    boot_and_test $2 $3 $4
done


