#! /usr/bin/env python

"""
"""

import time
import os
import sys
import argparse
import pickle
import numpy as np
from dsa2k_f import IwaveFengine
from matplotlib import pyplot as plt

DEFAULT_FPGFILE = '/home/jackh/src/dsa2000-fpga/firmware/src/models/dsa2k_iwave_adc_only_sync_test/outputs/dsa2k_iwave_adc_only_sync_test_2024-05-12_1749.fpg'
DEFAULT_HOSTS = '192.168.2.148,192.168.2.161'
DEFAULT_FPGA_CLK_HZ = 300000000
DEFAULT_SYNC_DELAY = 26

def load_tt(fs, fpga_clk_hz):
    tt = int(time.time() * fpga_clk_hz)
    for f in fs:
        f.sync.load_internal_time(tt, software_load=False, current_msb=None)
    ctrl = fs[0].sync.read_uint('ctrl')
    ctrl_arm_low  = ctrl & ~(1<<fs[0].sync.OFFSET_TT_INT_LOAD_ARM)
    ctrl_arm_high = ctrl | (1<<fs[0].sync.OFFSET_TT_INT_LOAD_ARM)
    for f in fs:
        f.sync.write_int('ctrl', ctrl_arm_low)
    c0 = fs[0].sync.wait_for_sync()
    #fs[0].sync.write_int('ctrl', ctrl_arm_high)
    #fs[1].sync.write_int('ctrl', ctrl_arm_high)
    for f in fs:
        f.sync.write_int('ctrl', ctrl_arm_high)
    c1 = fs[0].sync.count_ext()
    return c1 - c0

def capture_data(fs, fpga_clk_hz, delay=3):
    for i in range(40):
        try:
            curr_tt, curr_sync_count = fs[0].sync.get_tt_of_sync()
            break
        except RuntimeError:
            pass
    curr_ctime = time.ctime(curr_tt / fpga_clk_hz)
    tt_skew = (curr_tt / fpga_clk_hz) - time.time()
    print(f'Current TT is {curr_tt} ({curr_ctime})')
    print(f'Current skew is {tt_skew:.2f}')
    # Set a trigger for N seconds in the future
    target_tt = int((time.time() + delay) * fpga_clk_hz)
    delta_tt = target_tt - curr_tt
    delta_utc = delta_tt / fpga_clk_hz
    print(f'Target TT is {target_tt} (Delta: {delta_tt}, {delta_utc:.2f} seconds)')
    assert target_tt > curr_tt
    for f in fs:
        f.sync.load_timed_sync(target_tt)
        f.input._arm_snapshot()
    # wait for sync to pass
    time.sleep(delay + 1)
    for i in range(40):
        try:
            curr_tt, curr_sync_count = fs[0].sync.get_tt_of_sync()
            break
        except RuntimeError:
            pass
    delta_tt = target_tt - curr_tt
    delta_utc = delta_tt / fpga_clk_hz
    print(f'TT after waiting for sync is {curr_tt}')
    print(f'TT after waiting for sync is {curr_tt} (Delta: {delta_tt}, {delta_utc:.2f} seconds)')
    assert target_tt < curr_tt
    d = {}
    for f in fs:
        d[f.hostname] = f.input._read_snapshot()
    return d

def correlate(d0, d1, fpga_clk_hz, realfft=True, lo_mhz=0.0):
    if realfft:
        freqs_mhz = np.fft.fftfreq(len(d0), 1./(16*fpga_clk_hz))[0:len(d0)//2 + 1] / 1e6
        D0 = np.fft.rfft(d0)
        D1 = np.fft.rfft(d1)
    else:
        freqs_mhz = np.fft.fftfreq(len(d0), 1./(8*fpga_clk_hz)) / 1e6
        D0 = np.fft.fft(d0)
        D1 = np.fft.fft(d1)
    X = D0 * np.conj(D1)
    p = np.argmax(np.abs(X))
    f = freqs_mhz[p] + lo_mhz
    print(f'LO shift is {lo_mhz} MHz')
    print(f'Max power in bin {p} ({f:.2f} MHz)')
    timestamp = time.time()
    fbase = f'snapshot_{timestamp}'
    pklfile = f'{fbase}.pkl'
    print(f'Saving data {pklfile}')
    with open(pklfile, 'wb') as fh:
        pickle.dump([d0,d1], fh)
    return f, X[p]

def update_tt(f, fpga_clk_hz):
    for i in range(1,100):
        try:
            f.sync.update_internal_time(fs_hz=fpga_clk_hz, quiet=True)
            print(f'SYNC successful after {i} attempts')
            break
        except RuntimeError:
            continue

def plot_timing(fs):
    plt.figure()
    d = {}
    for fn, f in enumerate(fs):
        s = f._cfpga.snapshots.sync_ss
        x = np.array(s.read()['data']['data'])
        d[f.hostname] = x
        plt.subplot(2,1,fn+1)
        for i in [0,1,2,5,6,7]:
            plt.plot((x>>i) & 0b1,label=i)
        plt.legend()
    timestamp = time.time()
    fbase = f'timing_diagram_{timestamp}'
    pngfile = f'{fbase}.png'
    pklfile = f'{fbase}.pkl'
    print(f'Saving figure {pngfile}')
    plt.savefig(pngfile)
    with open(pklfile, 'wb') as fh:
        pickle.dump(d, fh)

def main(hosts, fpgfile, sync_delay, plot, realfft=True, lo_mhz=0.0, fpga_clk_hz=DEFAULT_FPGA_CLK_HZ):

    if not os.path.isfile(fpgfile):
        print(f"{fpgfile} doesn't exist. Exiting")
        exit()

    fs = []
    for host in hosts:
        print(f'Connecting to host {host}')
        fs += [IwaveFengine(host, has_ddc=not realfft)]

    for f in fs:
        f.program(fpgfile) # This doesn't actually program the FPGA, but loads register map.
        print(f'Setting sync delay to {sync_delay}')
        f.fpga.write_int('sync_delay', sync_delay) # empirical
        f.sync.initialize()

    time.sleep(2) # Wait a second to give error counters a chance
    for f in fs:
        errs = f.sync.get_period_variations()
        period_clks = f.sync.period()
        period_hz = fpga_clk_hz / period_clks
        print(f'{f.hostname}: sync period - {period_clks} FPGA clocks')
        print(f'{f.hostname}: sync period - {period_hz} Hz')
        print(f'{f.hostname}: sync period errors - {errs}')

    print(f'Updating internal time counters')
    for i in range(1, 200):
        x = load_tt(fs, fpga_clk_hz)
        if x == 0:
            print(f'SUCCESS after {i} attempts')
            break
        if x == 99:
            print(f'FAILED after {i} attempts')
            exit()

    d = capture_data(fs, fpga_clk_hz, delay=2)
    k = list(d.keys())
    freq_mhz, corr = correlate(d[k[0]][1], d[k[1]][1], fpga_clk_hz, realfft=realfft, lo_mhz=lo_mhz)
    phase = np.angle(corr)
    delay_ns = phase/(2*np.pi*freq_mhz*1e6) * 1e9
    
    print(k)
    if plot:
        plot_timing(fs)
    print(f'Correlation phase: {phase:.5f} ({delay_ns:.5f} ns)')
    
    #for hostname, data in d.items():
    #    for i in range(2):
    #        plt.plot(data[i][0:100], label=f'{hostname}:{i}')
    #plt.legend()
    #plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Capture ADC samples to csv file',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--fpgfile', type=str, default=DEFAULT_FPGFILE,
                        help='fpgfile running on FPGA')
    parser.add_argument('--hosts', type=str, default=DEFAULT_HOSTS,
                        help='Comma separated FPGA hostnames (or IP addresses) of FPGA boards')
    parser.add_argument('--sync_delay', type=int, default=DEFAULT_SYNC_DELAY,
                        help='Delay to apply to generated LMFC vs sync input')
    parser.add_argument('--complexfft', action='store_true',
                        help='If set, do a complex, rather than real, FFT')
    parser.add_argument('--lo_mhz', type=float, default=0.0,
                        help='If set, add this LO to the input frequency calculation')
    parser.add_argument('--fpga_clk_hz', type=int, default=DEFAULT_FPGA_CLK_HZ,
                        help='FPGA DSP clock in Hz')
    parser.add_argument('--plot', action='store_true',
                        help='Plot LMFC timing diagrams')
    
    args = parser.parse_args()
    main(args.hosts.split(','),
         args.fpgfile,
         args.sync_delay,
         args.plot,
         realfft = not args.complexfft,
         lo_mhz = args.lo_mhz,
         fpga_clk_hz = args.fpga_clk_hz,
    )
