#!/bin/bash

# Function to keep pinging a server until it is up
function wait_for_server() {
    local server_ip=$1
    local timeout=${2:-60}  # Default timeout is 60 seconds

    echo "Pinging server ${server_ip} until it is up..."

    # Loop until the ping is successful
    local start_time=$(date +%s)
    while true; do
        # Ping the server once (-c 1)
        if ping -c 1 $server_ip &> /dev/null; then
            echo "Server ${server_ip} is now up!"
            return 0
        else
            local current_time=$(date +%s)
            local elapsed_time=$((current_time - start_time))

            # Check if the timeout has been exceeded
            if ((elapsed_time > timeout)); then
                echo "Timeout reached. Server ${server_ip} is still down."
                return 1
            fi
        fi

        # Wait for 5 seconds before retrying
        sleep 5
    done
}

wait_for_server $1 $2
