# Libraries required for this project

## `adi_hdl`

Open-source HDL libraries from Analog Devices, supporting JES204 interfaces
and peripheral logic.

Use the included `Makefile` to build relevant library components.

## `mlib_devel`

Open-source DSP libraries and FPGA build toolchain, based around Mathworks
Simulink.

## `rtr_casper_dsp_lib`

High-level DSP modules build on top of the CASPER libraries by
Real-Time Radio Systems.
