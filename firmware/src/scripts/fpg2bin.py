#!/usr/bin/env python

import sys
import gzip

try:
    fnamein = sys.argv[1]
except IndexError:
    print('Usage: fpg2bin.py [fpgfile]')
    exit()

try:
    fin = open(fnamein, 'rb')
except:
    print(f'Error opening file {fnamein}')
    exit()

while(True):
    line = fin.readline()
    if line.startswith(b'#'):
        continue
    if line.startswith(b'?quit'):
        break

compressed = fin.read()
fin.close()

with open(f'{fnamein}.bin', 'wb') as fout:
    fout.write(gzip.decompress(compressed))

