/include/ "system-conf.dtsi"

#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/iio/adc/adi,ad9081.h>
#include <dt-bindings/jesd204/adxcvr.h>
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/iio/frequency/hmc7044.h>

/ {
  //chosen {
  //  bootargs = "earlycon console=ttyPS0,115200 clk_ignore_unused root=/dev/mmcblk1p2 rw rootwait ignore_loglevel log_buf_len=65536";
  //};

	regulators {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <0>;
		reg_ssd_vqmmc: regulator_ssd_vqmmc {
			compatible = "regulator-gpio";
			regulator-name = "ssd_vqmmc";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
			regulator-type = "voltage";
			gpios = <&gpio 43 1>;
			gpios-states = <1>;
			states = <3300000 1
				1800000 0>;
		};
	};

	fmc {
		/* 
		 * By default, FMC Connector power is disabled. 
		 * 1.8V compatibility is checked while booting by reading 
		 * FMC Module's EEPROM to enable correponding FMC connector power.
		 */
		fmc-vadj-millivolt = <1800>;
		fmc-prsnt-m2c = <&gpio3  0 GPIO_ACTIVE_HIGH>;
		fmc-vcc-adj   = <&gpio3  5 GPIO_ACTIVE_HIGH>;
		fmc-vcc-12v   = <&gpio3  3 GPIO_ACTIVE_HIGH>;
		fmc-vcc-3v3   = <&gpio3  4 GPIO_ACTIVE_HIGH>;
		fmc-pg-c2m    = <&gpio3 12 GPIO_ACTIVE_HIGH>;
	};

	fmc_plus {
		/* 
		 * By default, FMC+ Connector power is disabled. 
		 * 1.8V compatibility is checked while booting by reading 
		 * FMC+ Module's EEPROM to enable correponding FMC+ connector power.
		 */
		vadj-millivolt = <1800>;
		prsnt-m2c = <&gpio3  1 GPIO_ACTIVE_HIGH>;
		vcc-adj   = <&gpio3  6 GPIO_ACTIVE_HIGH>;
		vcc-12v   = <&gpio3  8 GPIO_ACTIVE_HIGH>;
		vcc-3v3   = <&gpio3  9 GPIO_ACTIVE_HIGH>;
		pg-c2m    = <&gpio3 13 GPIO_ACTIVE_HIGH>;
	};

        retimer_clk: retimer_clk {
                compatible = "fixed-clock";
                #clock-cells = <0>;
                clock-frequency = <148500000>;
        };

     	cpu_opp_table {

		opp00 {
			opp-hz = /bits/ 64 <329166666>;
		};
		opp01 {
                        opp-hz = /bits/ 64 <438888888>;
                };
		opp02 {
                        opp-hz = /bits/ 64 <658333333>;
                };
		opp03 {
                        opp-hz = /bits/ 64 <1316666666>;
                };
	};
	amba_pl: amba_pl@0 {
		#address-cells = <2>;
		#size-cells = <2>;
		compatible = "simple-bus";
		ranges ;
                //bom_rev {
                //        pl-aw17 = <&board_config_axi_gpio_4 0 0 0>;
                //        pl-bb16 = <&board_config_axi_gpio_4 1 0 0>;
                //        pl-ba16 = <&board_config_axi_gpio_4 2 0 0>;
                //        pl-ba17 = <&board_config_axi_gpio_4 3 0 0>;
                //        pl-ay17 = <&board_config_axi_gpio_4 4 0 0>;
                //};
	};

  //M_AXI0: memory@a0000000 {
  //    device_type = "memory";
  //    reg = < 0x0 0x2000000 >;
  //};
  fpga_axi: fpga-axi@0 {
		interrupt-parent = <&gic>;
		compatible = "simple-bus";
		#address-cells = <0x1>;
		#size-cells = <0x1>;
		ranges = <0 0 0 0xffffffff>;

		rx_dma: dma@9c420000 {
			compatible = "adi,axi-dmac-1.00.a";
			reg = <0x9c420000 0x0400>;
			#dma-cells = <1>;
			#clock-cells = <0>;
			interrupts = <0 109 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&zynqmp_clk 73>;
		};

		//tx_dma: dma@9c430000  {
		//	compatible = "adi,axi-dmac-1.00.a";
		//	reg = <0x9c430000 0x1000>;
		//	#dma-cells = <1>;
		//	#clock-cells = <0>;
		//	interrupts = <0 108 IRQ_TYPE_LEVEL_HIGH>;
		//	clocks = <&zynqmp_clk 73>;
		//};

		axi_ad9081_core_rx: axi-ad9081-rx-hpc@84a10000 {
			compatible = "adi,axi-ad9081-rx-1.0";
			reg = <0x84a10000 0x2000>;
			dmas = <&rx_dma 0>;
			dma-names = "rx";
			spibus-connected = <&trx0_ad9081>;

			jesd204-device;
			#jesd204-cells = <2>;
			jesd204-inputs = <&axi_ad9081_rx_jesd 0 FRAMER_LINK0_RX>;
		};

		//axi_ad9081_core_tx: axi-ad9081-tx-hpc@84b10000 {
		//	compatible = "adi,axi-ad9081-tx-1.0";
		//	reg = <0x84b10000 0x2000>;
		//	dmas = <&tx_dma 0>;
		//	dma-names = "tx";
		//	clocks = <&trx0_ad9081 1>;
		//	clock-names = "sampl_clk";
		//	spibus-connected = <&trx0_ad9081>;
		//	//adi,axi-pl-fifo-enable;
		//	adi,axi-data-offload-connected = <&axi_data_offload_tx>;

		//	jesd204-device;
		//	#jesd204-cells = <2>;
		//	jesd204-inputs = <&axi_ad9081_tx_jesd 0 DEFRAMER_LINK0_TX>;
		//};

		axi_ad9081_rx_jesd: axi-jesd204-rx@84a90000 {
			compatible = "adi,axi-jesd204-rx-1.0";
			reg = <0x84a90000 0x4000>;

			interrupts = <0 107 IRQ_TYPE_LEVEL_HIGH>;

			clocks = <&zynqmp_clk 71>, <&hmc7044 10>, <&axi_ad9081_adxcvr_rx 1>, <&axi_ad9081_adxcvr_rx 0>;
			clock-names = "s_axi_aclk", "device_clk", "link_clk", "lane_clk";

			#clock-cells = <0>;
			clock-output-names = "jesd_rx_lane_clk";

			jesd204-device;
			#jesd204-cells = <2>;
			jesd204-inputs = <&axi_ad9081_adxcvr_rx 0 FRAMER_LINK0_RX>;
		};

		//axi_ad9081_tx_jesd: axi-jesd204-tx@84b90000 {
		//	compatible = "adi,axi-jesd204-tx-1.0";
		//	reg = <0x84b90000 0x4000>;

		//	interrupts = <0 106 IRQ_TYPE_LEVEL_HIGH>;

		//	clocks = <&zynqmp_clk 71>, <&hmc7044 6>, <&axi_ad9081_adxcvr_tx 1>, <&axi_ad9081_adxcvr_tx 0>;
		//	clock-names = "s_axi_aclk", "device_clk", "link_clk", "lane_clk";

		//	#clock-cells = <0>;
		//	clock-output-names = "jesd_tx_lane_clk";

		//	jesd204-device;
		//	#jesd204-cells = <2>;
		//	jesd204-inputs = <&axi_ad9081_adxcvr_tx 0 DEFRAMER_LINK0_TX>;
		//};

		axi_ad9081_adxcvr_rx: axi-adxcvr-rx@84a60000 {
			#address-cells = <1>;
			#size-cells = <0>;
			compatible = "adi,axi-adxcvr-1.0";
			reg = <0x84a60000 0x10000>;

			clocks = <&hmc7044 12>;
			clock-names = "conv";

			#clock-cells = <1>;
			clock-output-names = "rx_gt_clk", "rx_out_clk";

			adi,sys-clk-select = <XCVR_QPLL>; //QPLL1 doesn't work for bitrates between 12.5 and 16 Gb/s (see https://docs.amd.com/r/en-US/ds926-zynq-ultrascale-plus-rfsoc/GTY-Transceiver-Switching-Characteristics)
			adi,out-clk-select = <XCVR_PROGDIV_CLK>;
			//adi,out-clk-select = <XCVR_REFCLK>;
 			adi,use-lpm-enable;

			jesd204-device;
			#jesd204-cells = <2>;
			jesd204-inputs =  <&hmc7044 0 FRAMER_LINK0_RX>;
		};

		//axi_ad9081_adxcvr_tx: axi-adxcvr-tx@84b60000 {
		//	#address-cells = <1>;
		//	#size-cells = <0>;
		//	compatible = "adi,axi-adxcvr-1.0";
		//	reg = <0x84b60000 0x10000>;

		//	clocks = <&hmc7044 12>;
		//	clock-names = "conv";

		//	#clock-cells = <1>;
		//	clock-output-names = "tx_gt_clk", "tx_out_clk";

		//	adi,sys-clk-select = <XCVR_QPLL1>;
		//	adi,out-clk-select = <XCVR_REFCLK>;
		//	//adi,out-clk-select = <XCVR_PROGDIV_CLK>;

		//	jesd204-device;
		//	#jesd204-cells = <2>;
		//	jesd204-inputs =  <&hmc7044 0 DEFRAMER_LINK0_TX>;
		//};

		axi_sysid_0: axi-sysid-0@85000000 {
			compatible = "adi,axi-sysid-1.00.a";
			reg = <0x85000000 0x10000>;
		};

		//axi_data_offload_tx: axi-data-offload-0@9c440000 {
		//	compatible = "adi,axi-data-offload-1.0.a";
		//	reg = <0x9c440000 0x4000>;
		//	// adi,bringup;
		//	// adi,oneshot;
		//	// adi,bypass;
		//	// adi,sync-config = <2>;
		//	// adi,transfer-length = /bits/ 64 <0x10000>; // 2**16 bytes
		//};

		axi_data_offload_rx: axi-data-offload-1@9c450000 {
			compatible = "adi,axi-data-offload-1.0.a";
			reg = <0x9c450000 0x4000>;
		};
	};

};

//&trx0_ad9081 {
//	reset-gpios = <&gpio 133 0>;
//	sysref-req-gpios = <&gpio 121 0>;
//	rx2-enable-gpios = <&gpio 135 0>;
//	rx1-enable-gpios = <&gpio 134 0>;
//	tx2-enable-gpios = <&gpio 137 0>;
//	tx1-enable-gpios = <&gpio 136 0>;
//};

&can0 {
	status = "okay";
	pwdn-gpio = <&gpio 80 0>;
};


&gem0 {
	phy-mode = "rgmii-id";
	local-mac-address = [00 01 02 03 04 05];
	status = "okay";
	xlnx,ptp-enet-clock = <0x0>;
	phy-handle = <&phy0>;
	phy-reset-gpio = <&gpio 42 1>;
	phy-reset-active-low;
	phy-reset-duration = <20>;
	phy0: phy@1 {
		reg = <1>;
		ti,rx-internal-delay = <0x8>;
		ti,tx-internal-delay = <0xa>;
		ti,fifo-depth = <0x1>;
		ti,rxctrl-strap-worka;
		at803x,led-act-blind-workaround;
		at803x,eee-disabled;
		at803x,vddio-1p8v;
		interrupt-parent = <&gpio>;
		interrupts = <12 8>;
	};
};

&i2c0 {
	clock-frequency = <400000>;
	status = "okay";
	
	pmic0: da9062@58 {
		compatible = "dlg,da9062";
		reg = <0x58>;
		interrupt-parent = <&gpio>;
		interrupts = <2 8>;
		interrupt-controller;
		rtc {
			compatible = "dlg,da9062-rtc";
		};
	};

  fusb302: typec-portc@22 {
		compatible = "fcs,fusb302";
		reg = <0x22>;
		fcs,int_n = <&gpio 78 8>;
		fcs,cc = <&gpio 79 0>;
		fcs,power_en = <&gpio 25 0>;
		status = "okay";
       };
	/* Programmable clock (for logic) */
	si5341: clock-generator@76 {
		reg = <0x76>;
		compatible = "silabs,si5341";
		#clock-cells = <2>;
		#address-cells = <1>;
		#size-cells = <0>;
		clocks = <&ref48>;
		clock-names = "xtal";
		clk0 {
			reg = <0>;
			always-on;
		};
		clk1 {
			reg = <1>;
			always-on;
		};
		clk2 {
			reg = <2>;
			always-on;
		};
		clk3 {
			reg = <3>;
			always-on;
		};
		clk4 {
			reg = <4>;
			always-on;
		};
		clk5 {
			reg = <5>;
			always-on;
		};
		clk6 {
			reg = <6>;
			always-on;
		};
		clk7 {
			reg = <7>;
			always-on;
		};
		clk9 {
			reg = <9>;
			always-on;
		};
	};

		i2c_mux: msd9546@70 {
		compatible = "pi4,msd9546";
		reg = <0x70>;
		status = "okay";
                #address-cells = <1>;
                #size-cells = <0>;

                i2c@0 {
                        #address-cells = <1>;
                        #size-cells = <0>;
                        reg = <0>;

                        gpio3: gpio@23 {
                                compatible = "ti,tca9535";
                                reg = <0x23>; 
                                #gpio-cells = <2>;
                                gpio-controller;
                        };
                };

                i2c@1 {
                        #address-cells = <1>;
                        #size-cells = <0>;
                        reg = <1>;
                };

                i2c@2 {
                        #address-cells = <1>;
                        #size-cells = <0>;
                        reg = <2>;

                        gpio1: gpio@20 {
		                compatible = "ti,tca6416";
                                reg = <0x20>; 
                                #gpio-cells = <2>;
                                gpio-controller;
                        };

                        gpio2: gpio@21 {
               			compatible = "ti,tca6416";
                                reg = <0x21>; 
                                #gpio-cells = <2>;
                                gpio-controller;
			};
                };

                i2c@3 {
                        #address-cells = <1>;
                        #size-cells = <0>;
                        reg = <3>;
                };

       };

};

&usb0 {
	status = "okay";
	xlnx,usb-reset = <0x2faf080>;
};

&sdhci0{
	bus-width = <8>;
};

&sdhci1 {
	clock-frequency = <200000000>;
	status = "okay";
	bus-width = <4>;
	xlnx,mio_bank = <0x1>;
	xlnx,has-cd = <0x1>;
	vqmmc-supply = <&reg_ssd_vqmmc>;
};

&rtc {
      status = "disabled";
};

&amba_pl {
        ref48: ref48M {
                compatible = "fixed-clock";
                #clock-cells = <0>;
                clock-frequency = <48000000>;
        };

        xlnk {
                compatible = "xlnx,xlnk-1.0";
        };

        //vcu_apm: apm@0xa0140000 {
        //        #address-cells = <0x1>;
        //        #size-cells = <0x1>;
        //        compatible = "generic-uio";
        //        reg = <0x0 0xa0140000 0x0 0x10000>;
        //        reg-names = "generic";
        //};
        casper_io: casper_io@a0000000 {
                #address-cells = <0x1>;
                #size-cells = <0x1>;
                compatible = "generic-uio";
                reg = <0x0 0xa0000000 0x0 0x2000000>;
                reg-names = "generic";
        };

};

&fclk0 {
       status = "okay";
};

&fclk1 {
       status = "okay";
};

&fclk2 {
       status = "okay";
};

&fclk3 {
       status = "okay";
};

&spi1 {
	status = "okay";

	hmc7044: hmc7044@0 {
		#address-cells = <1>;
		#size-cells = <0>;
		#clock-cells = <1>;
		compatible = "adi,hmc7044";
		reg = <0>;
		spi-max-frequency = <1000000>;

		jesd204-device;
		#jesd204-cells = <2>;
		jesd204-sysref-provider;

		adi,jesd204-max-sysref-frequency-hz = <2343750>; /* 2 MHz */
		//adi,pll1-ref-prio-ctrl = <0x1>; /* Use CLK1 if available, else CLK0 (see HMC7044 reg 0x14) */

		/*
		* There are different versions of the AD9081-FMCA-EBZ & AD9082-FMCA-EBZ
		* VCXO = 122.880 MHz, XO = 122.880MHz (AD9081-FMC-EBZ & AD9082-FMC-EBZ)
		* VCXO = 100.000 MHz, XO = 100.000MHz (AD9081-FMC-EBZ-A2 & AD9082-FMC-EBZ-A2)
		* To determine which board is which, read the freqency printed on the VCXO
		* or use the fru-dump utility:
		* #fru-dump -b /sys/bus/i2c/devices/15-0050/eeprom
		*/

		//adi,pll1-clkin-frequencies = <122880000 30720000 0 0>;
		//adi,vcxo-frequency = <122880000>;

		adi,pll1-clkin-frequencies = <100000000 100000000 0 0>;
		adi,vcxo-frequency = <100000000>;

		adi,pll1-loop-bandwidth-hz = <200>;

		adi,pll2-output-frequency = <2400000000>;

		adi,sysref-timer-divider = <1024>;
		adi,pulse-generator-mode = <0>;

		adi,clkin0-buffer-mode  = <0x07>;
		adi,clkin1-buffer-mode  = <0x07>;
		adi,oscin-buffer-mode = <0x15>;

		adi,gpi-controls = <0x00 0x00 0x00 0x00>;
		adi,gpo-controls = <0x37 0x33 0x00 0x00>;

		clock-output-names =
		"hmc7044_out0", "hmc7044_out1", "hmc7044_out2",
		"hmc7044_out3", "hmc7044_out4", "hmc7044_out5",
		"hmc7044_out6", "hmc7044_out7", "hmc7044_out8",
		"hmc7044_out9", "hmc7044_out10", "hmc7044_out11",
		"hmc7044_out12", "hmc7044_out13";

		hmc7044_c0: channel@0 { // unused
			reg = <0>;
			adi,extended-name = "CORE_CLK_RX";
			adi,divider = <16>; // 150 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
			adi,disable;
		};
		hmc7044_c2: channel@2 {
			reg = <2>;
			adi,extended-name = "DEV_REFCLK";
			adi,divider = <16>; // 150 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
		};
		hmc7044_c3: channel@3 {
			reg = <3>;
			adi,extended-name = "DEV_SYSREF";
			adi,divider = <1536>; // 0.78125 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
			adi,jesd204-sysref-chan;
		};

		hmc7044_c6: channel@6 {
			reg = <6>;
			adi,extended-name = "CORE_CLK_TX";
			adi,divider = <12>; // 200 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
		};

		hmc7044_c8: channel@8 { // UNUSED
			reg = <8>;
			adi,extended-name = "FPGA_REFCLK1";
			adi,divider = <8>; // 300 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
			adi,disable;
		};
		hmc7044_c10: channel@a {
			reg = <10>;
			adi,extended-name = "CORE_CLK_RX_ALT";
			adi,divider = <12>; // 200 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
		};

		hmc7044_c12: channel@c {
			reg = <12>;
			adi,extended-name = "FPGA_REFCLK2";
			adi,divider = <12>; // 200 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
		};
		hmc7044_c13: channel@d {
			reg = <13>;
			adi,extended-name = "FPGA_SYSREF";
			adi,divider = <1536>; // 0.78125 MHz
			adi,driver-mode = <HMC7044_DRIVER_MODE_LVDS>;
			adi,jesd204-sysref-chan;
                        //adi,disable;
		};
	};
};

&spi0 {
	trx0_ad9081: ad9082@0 {
	  reset-gpios = <&gpio 133 0>;
	  sysref-req-gpios = <&gpio 121 0>;
	  rx2-enable-gpios = <&gpio 135 0>;
	  rx1-enable-gpios = <&gpio 134 0>;
	  tx2-enable-gpios = <&gpio 137 0>;
	  tx1-enable-gpios = <&gpio 136 0>;
		#address-cells = <1>;
		#size-cells = <0>;
		compatible = "adi,ad9082";
		reg = <0>;
		spi-max-frequency = <5000000>;

		/* Clocks */
		clocks = <&hmc7044 2>;
		clock-names = "dev_clk";

		clock-output-names = "rx_sampl_clk", "tx_sampl_clk";
		#clock-cells = <1>;

		jesd204-device;
		#jesd204-cells = <2>;
		jesd204-top-device = <0>; /* This is the TOP device */
		//jesd204-link-ids = <FRAMER_LINK0_RX DEFRAMER_LINK0_TX>;
		jesd204-link-ids = <FRAMER_LINK0_RX>;

		//jesd204-inputs =
		//	<&axi_ad9081_core_rx 0 FRAMER_LINK0_RX>,
		//	<&axi_ad9081_core_tx 0 DEFRAMER_LINK0_TX>;
		jesd204-inputs =
			<&axi_ad9081_core_rx 0 FRAMER_LINK0_RX>;

		adi,continuous-sysref-mode-disable;

		adi,tx-dacs {
			#size-cells = <0>;
			#address-cells = <1>;

			adi,dac-frequency-hz = /bits/ 64 <9600000000>;
			//adi,dac-frequency-hz = /bits/ 64 <4000000000>;
		};

		adi,rx-adcs {
			#size-cells = <0>;
			#address-cells = <1>;

			adi,adc-frequency-hz = /bits/ 64 <4800000000>;
			//adi,adc-frequency-hz = /bits/ 64 <4000000000>;

			adi,main-data-paths {
				#address-cells = <1>;
				#size-cells = <0>;

				ad9081_adc0: adc@0 {
					reg = <0>;
					adi,decimation = <3>;
					adi,nco-frequency-shift-hz =  /bits/ 64 <1350000000>;
					adi,nco-mixer-mode = <AD9081_ADC_NCO_VIF>; // Could use AD9081_ADC_NCO_FS_4_IF for Fs/4
				};
				ad9081_adc1: adc@1 {
					reg = <1>;
					adi,decimation = <3>;
					adi,nco-frequency-shift-hz =  /bits/ 64 <1350000000>;
					adi,nco-mixer-mode = <AD9081_ADC_NCO_VIF>; // Could use AD9081_ADC_NCO_FS_4_IF for Fs/4
				};

			};

			adi,channelizer-paths {
				#address-cells = <1>;
				#size-cells = <0>;


				ad9081_rx_fddc_chan0: channel@0 {
					reg = <0>;
					adi,decimation = <1>;
					adi,gain = <2048>; /* 2048 * 10^(gain_dB/20) */
					adi,nco-frequency-shift-hz =  /bits/ 64 <0>;

				};
				ad9081_rx_fddc_chan1: channel@1 {
					reg = <1>;
					adi,decimation = <1>;
					adi,gain = <2048>; /* 2048 * 10^(gain_dB/20) */
					adi,nco-frequency-shift-hz =  /bits/ 64 <0>;

				};
			};

			adi,jesd-links {
				#size-cells = <0>;
				#address-cells = <1>;

				ad9081_rx_jesd_l0: link@0 {
					reg = <0>;
					adi,converter-select =
						 <&ad9081_rx_fddc_chan0 FDDC_I>, <&ad9081_rx_fddc_chan0 FDDC_Q>,
						 <&ad9081_rx_fddc_chan1 FDDC_I>, <&ad9081_rx_fddc_chan1 FDDC_Q>;

                                        adi,logical-lane-mapping = /bits/ 8 <2 0 7 6 5 4 3 1>;

					adi,link-mode = <18>;			/* JESD Quick Configuration Mode */
					adi,subclass = <1>;			/* JESD SUBCLASS 0,1,2 */
					adi,version = <2>;			/* JESD VERSION 0=204A,1=204B,2=204C */
					adi,dual-link = <0>;			/* JESD Dual Link Mode */

					adi,converters-per-device = <4>;	/* JESD M */
					adi,octets-per-frame = <1>;		/* JESD F */

					adi,frames-per-multiframe = <256>;	/* JESD K */
					adi,converter-resolution = <16>;	/* JESD N */
					adi,bits-per-sample = <16>;		/* JESD NP' */
					adi,control-bits-per-sample = <0>;	/* JESD CS */
					adi,lanes-per-device = <8>;		/* JESD L */
					adi,samples-per-converter-per-frame = <1>; /* JESD S */
					adi,high-density = <1>;			/* JESD HD */
				};
			};
		};
	};
};
