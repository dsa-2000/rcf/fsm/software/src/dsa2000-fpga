#!/bin/bash

voltage_raw=`cat /sys/bus/iio/devices/iio:device1/in_voltage1_raw`
voltage_scale=`cat /sys/bus/iio/devices/iio:device1/in_voltage1_scale`
voltage=$(echo "scale=1; $voltage_raw * $voltage_scale / 1000" | bc)
echo -n "Voltage (V): "
echo $voltage

current_raw=`cat /sys/bus/iio/devices/iio:device1/in_current0_raw`
current_scale=`cat /sys/bus/iio/devices/iio:device1/in_current0_scale`
current=$(echo "scale=1; $current_raw * $current_scale / 1000" | bc)
echo -n "Current (A): "
echo $current

power_raw=`cat /sys/bus/iio/devices/iio:device1/in_power2_raw`
power_scale=`cat /sys/bus/iio/devices/iio:device1/in_power2_scale`
power=$(echo "scale=1; $power_raw * $power_scale / 1000" | bc)
echo -n "Power (W): "
echo $power

temp_raw=`cat /sys/class/hwmon/hwmon0/temp1_input`
temp=$(echo "scale=1; $temp_raw / 1000" | bc)
echo -n "Temp0 (C): "
echo $temp

temp_raw=`cat /sys/class/hwmon/hwmon1/temp1_input`
temp=$(echo "scale=1; $temp_raw / 1000" | bc)
echo -n "Temp1 (C): "
echo $temp
