import adi_reg

sysref_err_win = adi_reg.ad9082_spi_read(0xb7, 0, 7)
sysref_win_ok = adi_reg.ad9082_spi_read(0xb7, 7, 1)
print(f'Sysref error window: {sysref_err_win} clocks')
print(f'Sysref within window? {sysref_win_ok}')

adi_reg.ad9082_spi_write(0xb5, 0x0) # Trigger phase capture (write anything)
sysref_phase = (adi_reg.ad9082_spi_read(0xb6) << 8) + adi_reg.ad9082_spi_read(0xb5)
print(f'SYSREF vs LEMC phase: {sysref_phase}')

sysref_setup = adi_reg.ad9082_spi_read(0xfb7)
sysref_hold  = adi_reg.ad9082_spi_read(0xfb8)
print(f'SYSREF setup: {bin(sysref_setup)}')
print(f'SYSREF hold: {bin(sysref_hold)}')
