import spidev

spi = spidev.SpiDev()
spi.open(1,0)

def write(spi, addr, v):
    addr = addr + (0 << 15)
    x = [((addr >> 8) & 0xff), (addr & 0xff), v]
    return spi.xfer2(x)

def read(spi, addr):
    addr = addr + (1 << 15)
    x = [((addr >> 8) & 0xff), (addr & 0xff), 0x0]
    return spi.xfer2(x)[2]

print('Max speed:', spi.max_speed_hz)
print('Mode:', spi.mode)
print('Bits per word::', spi.bits_per_word)
print('CS high:', spi.cshigh)
print('Loop:', spi.loop)
print('No CS:', spi.no_cs)
print('LSB First:', spi.lsbfirst)
print('3WIRE:', spi.threewire)

#print(write(spi, 0x0, 1))
print(hex(read(spi, 0x03)))
