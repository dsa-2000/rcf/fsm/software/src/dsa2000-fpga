#! /usr/bin/env python

import spidev
import sys

spi = spidev.SpiDev()
spi.open(0,2)
spi.mode = 0
spi.max_speed_hz = 2000000

def print_spi_status(spi):
    print('Max speed:', spi.max_speed_hz)
    print('Mode:', spi.mode)
    print('Bits per word::', spi.bits_per_word)
    print('CS high:', spi.cshigh)
    print('Loop:', spi.loop)
    print('No CS:', spi.no_cs)
    print('LSB First:', spi.lsbfirst)
    print('3WIRE:', spi.threewire)

def write(spi, addr, v):
    addr = addr + (0 << 15)
    x = [((addr >> 8) & 0xff), (addr & 0xff), v]
    print('WRITE:', [hex(n) for n in x])
    return spi.xfer2(x)

def read(spi, addr):
    addr = addr + (1 << 15)
    x = [((addr >> 8) & 0xff), (addr & 0xff), 0x0]
    rv = spi.xfer2(x)
    print(rv)
    return int(rv[2])

def raw_write(spi, val):
    x = [(val >> 16) & 0xff, (val >> 8) & 0xff, val & 0xff]
    print('WRITE:', [hex(n) for n in x])
    return spi.xfer2(x)

def print_help():
    print('Usage: spi_ad9082_send.py hexfile.txt')

if len(sys.argv) != 2:
    print_help()
    print_spi_status(spi)
    exit()
    
with open(sys.argv[1], 'r') as fh:
    for line in fh.readlines():
        a, v = line.split('\t')
        v = int(v, 16)
        raw_write(spi, v)
