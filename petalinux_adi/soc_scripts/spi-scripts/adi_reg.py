import subprocess

HMC7044_IIO_ID = 1
AD9082_IIO_ID = 2

def ad_spi_read(iio_id, addr, offset=0, width=8, verbose=False):
    mask = ((1 << width) - 1)
    write_command = f"echo {addr:#x} > /sys/kernel/debug/iio/iio:device{iio_id}/direct_reg_access"
    subprocess.run(write_command, shell=True, check=True)  # Set the address to read
    read_command = f"cat /sys/kernel/debug/iio/iio:device{iio_id}/direct_reg_access"
    result = subprocess.run(read_command, shell=True, capture_output=True, check=True, text=True)
    full_value = int(result.stdout.strip(), 16)
    specific_bits_value = (full_value >> offset) & mask
    if verbose:
        print(f"SPI Read: Addr={addr:#x}, Value={specific_bits_value:#x} with mask {mask:#x}")
    return specific_bits_value

def ad_spi_write(iio_id, addr, value, offset=0, width=8, verbose=False):
    mask = ((1 << width) - 1) << offset
    # Read current value
    current_val = ad9082_spi_read(addr, offset=0, width=8)
    # Clear the bits of interest
    current_val &= ~mask
    # Set the new bits
    value_shifted = (value << offset) & mask
    new_val = current_val | value_shifted

    write_command = f"echo {addr:#x} {new_val:#x} > /sys/kernel/debug/iio/iio:device{iio_id}/direct_reg_access"
    subprocess.run(write_command, shell=True, check=True)
    if verbose:
        print(f"SPI Write: Addr={addr:#x}, Value={new_val:#x} with mask {mask:#x}")

def ad9082_spi_read(addr, offset=0, width=8, verbose=False):
    return ad_spi_read(AD9082_IIO_ID, addr, offset=offset, width=width, verbose=False)

def ad9082_spi_write(addr, value, offset=0, width=8, verbose=False):
    return ad_spi_write(AD9082_IIO_ID, addr, value, offset=offset, width=width, verbose=False)

def hmc7044_spi_read(addr, offset=0, width=8, verbose=False):
    return ad_spi_read(HMC7044_IIO_ID, addr, offset=offset, width=width, verbose=False)

def hmc7044_spi_write(addr, value, offset=0, width=8, verbose=False):
    return ad_spi_write(HMC7044_IIO_ID, addr, value, offset=offset, width=width, verbose=False)
