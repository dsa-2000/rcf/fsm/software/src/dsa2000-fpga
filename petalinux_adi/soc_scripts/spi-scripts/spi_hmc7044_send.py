#! /usr/bin/env python

import spidev
import sys

spi = spidev.SpiDev()
spi.open(1,0)

def print_spi_status(spi):
    # AD9081 is SPI mode 3
    print('Max speed:', spi.max_speed_hz)
    print('Mode:', spi.mode)
    print('Bits per word::', spi.bits_per_word)
    print('CS high:', spi.cshigh)
    print('Loop:', spi.loop)
    print('No CS:', spi.no_cs)
    print('LSB First:', spi.lsbfirst)
    print('3WIRE:', spi.threewire)

def write(spi, addr, v):
    addr = addr + (0 << 15)
    x = [((addr >> 8) & 0xff), (addr & 0xff), v]
    #print('WRITE:', [hex(n) for n in x])
    return spi.xfer2(x)

def read(spi, addr):
    addr = addr + (1 << 15)
    x = [((addr >> 8) & 0xff), (addr & 0xff), 0x0]
    rv = spi.xfer2(x)
    return int(rv[2])

def print_help():
    print('Usage: spi_hmc7044_send.py hex_addr [hex_value]')
    print('Eg. spi_hmc7044_send.py 0x0 0x3')

if len(sys.argv) < 2:
    print_help()
    exit()
    
try:
    addr = int(sys.argv[1], 16)
except:
    print_help()
    raise

if len(sys.argv) > 2:
    try:
        val = int(sys.argv[2], 16)
    except:
        print_help()
        raise
    write(spi, addr, val)
else:
    print(hex(read(spi, addr)))

