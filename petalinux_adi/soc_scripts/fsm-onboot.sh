#/home/casper/src/katcp/tcpborphserver3/tcpborphserver3

CLOCKFILE=/home/casper/src/dsa2000-fpga/petalinux_adi/soc_scripts/spi-scripts/iWaveFsmLmkHex-Onboard.txt
#CLOCKFILE=/home/casper/src/dsa2000-fpga/petalinux_adi/soc_scripts/spi-scripts/iWaveFsmLmkHex.txt

# Turn off ADC
# Sync PLL
# Turn on ADC
echo 'Powering down ADC'
echo 1 > /sys/bus/iio/devices/iio:device3/powerdown
echo 'Programming PLL'
/usr/bin/python /home/casper/src/dsa2000-fpga/petalinux_adi/soc_scripts/spi-scripts/spi_program_lmk_hexfile.py $CLOCKFILE
sleep 1
echo 'Powering up ADC'
echo 0 > /sys/bus/iio/devices/iio:device3/powerdown
#echo 'Waiting for internet before updating NTP'
#until ping -nq -c3 8.8.8.8; do
#	echo "Waiting for network..."
#done
echo "Stopping NTP"
/usr/sbin/service ntp stop
echo "Forcing NTP update"
/usr/sbin/ntpd -gq
echo "Starting NTP"
/usr/sbin/service ntp start
