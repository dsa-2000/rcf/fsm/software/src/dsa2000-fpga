#! /usr/bin/env python

import sys
import time
import smbus

I2CBUS = 0
I2CADDR = 0x75


with open(sys.argv[1], 'r') as fh:
    lines = fh.read().split('\n')

print("Read %d lines" % len(lines))

print("Opening I2C bus %d" % I2CBUS)

iic = smbus.SMBus(I2CBUS)

page = None
PAGEADDR = 0x1
for line in lines:
    if line.startswith('#'):
        continue
    if line.startswith('Address'):
        continue
    if len(line) == 0:
        continue
    addr, data = line.split(',')
    addr = int(addr, 16)
    data = int(data, 16)
    page_req = addr >> 8
    addr_in_page = addr & 0xff
    ready = iic.read_byte_data(I2CADDR, 0xFE)
    if ready != 0x0f:
        raise RuntimeError
    if not page == page_req:
        print('Writing I2C device 0x%x addr 0x%x data 0x%x (page change)' % (I2CADDR, PAGEADDR, page_req))
        iic.write_byte_data(I2CADDR, PAGEADDR, page_req)
        page = page_req
    print('Writing I2C device 0x%x addr 0x%x data 0x%x' % (I2CADDR, addr, data))
    iic.write_byte_data(I2CADDR, addr_in_page, data)
    readback = iic.read_byte_data(I2CADDR, addr_in_page)
    if not readback == data:
        print('Readback did not match and was: 0x%x' % readback)
    time.sleep(0.01)
