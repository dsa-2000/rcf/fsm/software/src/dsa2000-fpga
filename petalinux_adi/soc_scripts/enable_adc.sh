#! /bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

if [ -d "/sys/class/gpio/gpio471" ]; then
    echo 471 > /sys/class/gpio/unexport
fi
echo 471 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio471/direction
echo 1 > /sys/class/gpio/gpio471/value

# Enable 4-wire SPI
${SCRIPT_DIR}/spi-scripts/spi_ad9081_send.py 0x0 0x99
