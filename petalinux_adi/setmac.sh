# Usage: sudo setmac.sh 02:02:33:33:00:35 # Set eth0 mac to 02:02:33:33:00:35 on next boot

echo "Setting MAC to $1"

sed "s/00:01:02:03:04:05/$1/" uboot.env.default.txt > uboot.env.txt
mkenvimage -s 262144 -o uboot.env uboot.env.txt
sync
